const app = require("./src/app");
const mongoConnect = require("./src/mongoose");

(async () => {
  try {
    // Start MongoDb connection
    await mongoConnect();
    // Start apps
    app(5000);
  } catch (error) {
    console.log(error);
    console.log(`error app can't running`);
  }
})();
