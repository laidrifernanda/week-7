const mongoose = require("mongoose");

(async () => {
  // Connect to mongodb server
  await mongoose.connect("mongodb://localhost:27017/binaracademy", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });

  // Init mongoose schema
  const Schema = mongoose.Schema;

  // Create Schema Users (Migrations)
  const Users = new Schema({
    fullName: String,
    age: Number,
    gender: String,
    address: String,
  });

  // Create models users
  const UsersModel = mongoose.model("users", Users);

  // // Insert new data
  await UsersModel.create(
    {
      fullName: "Fernanda Laidri",
      age: 26,
      gender: "L",
      address: "Palembang",
    },
    {
      fullName: "Fikhi Indrawan",
      age: 27,
      gender: "L",
      address: "Palembang",
    },
    {
      fullName: "Jordy Hutauruk",
      age: 27,
      gender: "L",
      address: "Palembang",
    },
    {
      fullName: "Ayu Tri",
      age: 28,
      gender: "P",
      address: "Bogor",
    },
    {
      fullName: "Melia Sari",
      age: 28,
      gender: "P",
      address: "Jambi",
    },
    {
      fullName: "Imammuslim Amin",
      age: 27,
      gender: "L",
      address: "Tangerang",
    },
    {
      fullName: "Made Dani",
      age: 34,
      gender: "L",
      address: "Bandar Lampung",
    },
    {
      fullName: "Febriantoro",
      age: 28,
      gender: "L",
      address: "Lahat",
    },
    {
      fullName: "Pebi Dian",
      age: 27,
      gender: "L",
      address: "Bandung",
    },
    {
      fullName: "Prima Oktapiansyah",
      age: 28,
      gender: "L",
      address: "Lubuk Linggau",
    }
  );

  // Get existing data
  // const data = await UsersModel.find({});
  // console.log(data);

  // update data
  // await UsersModel.updateOne({ _id: "600edc757f502f1285517cd0" }, { age: 27 });
  // const data = await UsersModel.find({});
  // console.log(data);

  // Delete Data
  await UsersModel.deleteOne({ _id: "600eef167cb9c9232f716a19" });
  const data = await UsersModel.find({});
  console.log(data);
})();
